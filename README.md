# EasyPeasy Web

This is the git repository for the [EasyPeasy website](https://pmohackbook.org), written in CSS grid and building the most recent version of the [latex document](https://gitlab.com/snuggy/easypeasy) using Gitlab CI.
